
import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/user_model.dart';

class AddUser extends AppEvent {
  User newUser;

  AddUser(User user) {
    newUser = user;
  }
}