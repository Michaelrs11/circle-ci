import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/user_model.dart';

class UpdateUser extends AppEvent {
  User newUser;
  int userIdx;

  UpdateUser(int idx, User user) {
    newUser = user;
    userIdx = idx;
  }
}