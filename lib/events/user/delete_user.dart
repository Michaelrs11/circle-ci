import 'package:circleciapp/events/app_event.dart';

class DeleteUser extends AppEvent {
  int userIdx;

  DeleteUser(int index) {
    userIdx = index;
  }
}