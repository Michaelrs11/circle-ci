import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/user_model.dart';

class InitUser extends AppEvent {
  List<User> userList;

  InitUser(List<User> user) {
    userList = user;
  }
}