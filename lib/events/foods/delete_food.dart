import 'package:circleciapp/events/app_event.dart';

class DeleteFood extends AppEvent {
  int foodIdx;

  DeleteFood(int index) {
    foodIdx = index;
  }
}