import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/food_model.dart';

class InitFood extends AppEvent {
  List<Food> foodList;

  InitFood(List<Food> food) {
    foodList = food;
  }
}