import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/food_model.dart';

class UpdateFood extends AppEvent {
  Food newFood;
  int foodIdx;

  UpdateFood(int idx, Food food) {
    newFood = food;
    foodIdx = idx;
  }
}