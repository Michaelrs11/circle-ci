import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/food_model.dart';

class AddFood extends AppEvent {
  Food newFood;

  AddFood(Food food) {
    newFood = food;
  }
}