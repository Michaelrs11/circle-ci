import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/purchase.dart';

class AddPurchase extends AppEvent {
  Purchase newPurchase;

  AddPurchase(Purchase purchase) {
    newPurchase = purchase;
  }
}