import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/purchase.dart';

class UpdatePurchase extends AppEvent {
  Purchase newPurchase;
  int purchaseIdx;

  UpdatePurchase(int idx, Purchase purchase) {
    newPurchase = purchase;
    purchaseIdx = idx;
  }
}