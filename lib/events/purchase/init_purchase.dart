import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/purchase.dart';

class InitPurchase extends AppEvent {
  List<Purchase> purchaseList;

  InitPurchase(List<Purchase> purchase) {
    purchaseList = purchase;
  }
}