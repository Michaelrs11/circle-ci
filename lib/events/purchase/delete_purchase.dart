import 'package:circleciapp/events/app_event.dart';

class DeletePurchase extends AppEvent {
  int purchaseIdx;

  DeletePurchase(int index) {
    purchaseIdx = index;
  }
}