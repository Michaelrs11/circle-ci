import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/purchase_detail.dart';

class UpdatePurchaseDetail extends AppEvent {
  PurchaseDetail newDetail;
  int detailIdx;

  UpdatePurchaseDetail(int idx, PurchaseDetail detail) {
    newDetail = detail;
    detailIdx = idx;
  }
}