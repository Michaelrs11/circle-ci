import 'package:circleciapp/events/app_event.dart';

class DeletePurchaseDetail extends AppEvent {
  int detailIdx;

  DeletePurchaseDetail(int index) {
    detailIdx = index;
  }
}