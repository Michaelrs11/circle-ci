import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/purchase_detail.dart';

class AddPurchaseDetail extends AppEvent {
  PurchaseDetail newDetail;

  AddPurchaseDetail(PurchaseDetail detail) {
    newDetail = detail;
  }
}