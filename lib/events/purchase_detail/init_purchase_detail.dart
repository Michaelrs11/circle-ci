import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/models/purchase_detail.dart';

class InitPurchaseDetail extends AppEvent {
  List<PurchaseDetail> detailList;

  InitPurchaseDetail(List<PurchaseDetail> detail) {
    detailList = detail;
  }
}