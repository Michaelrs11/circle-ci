import 'package:circleciapp/database/database_provider.dart';

class Purchase {
  String date;
  int id, userId;
  Purchase({this.id, this.userId, this.date});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DatabaseProvider.COLUMN_PURCHASE_USER_ID: userId,
      DatabaseProvider.COLUMN_PURCHASE_DATE: date,
    };

    if (id != null) {
      map[DatabaseProvider.COLUMN_PURCHASE_ID] = id;
    }

    return map;
  }

  Purchase.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseProvider.COLUMN_PURCHASE_ID];
    userId = map[DatabaseProvider.COLUMN_PURCHASE_USER_ID];
    date = map[DatabaseProvider.COLUMN_PURCHASE_DATE];
  }
}
