import 'package:circleciapp/database/database_provider.dart';

class Food{
  int id;
  String name;
  double price;
  int stock;

  Food({this.id, this.name, this.price, this.stock});

  Map<String, dynamic> toMap(){
    var map = <String, dynamic>{
      DatabaseProvider.COLUMN_NAME: name,
      DatabaseProvider.COLUMN_PRICE: price,
      DatabaseProvider.COLUMN_STOCK: stock,
    };

    if(id != null){
      map[DatabaseProvider.COLUMN_ID] = id;
    }

    return map;
  }

  Food.fromMap(Map<String, dynamic> map){
    id = map[DatabaseProvider.COLUMN_ID];
    name = map[DatabaseProvider.COLUMN_NAME];
    price = map[DatabaseProvider.COLUMN_PRICE].toDouble();
    stock = map[DatabaseProvider.COLUMN_STOCK];
  }
}