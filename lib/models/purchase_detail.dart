import 'package:circleciapp/database/database_provider.dart';

class PurchaseDetail {
  int id, purchaseId, productId, qty;
  PurchaseDetail({this.id, this.purchaseId, this.productId, this.qty});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DatabaseProvider.COLUMN_PURCHASE_DETAIL_PRODUCT_ID: productId,
      DatabaseProvider.COLUMN_PURCHASE_DETAIL_PURCHASE_ID: purchaseId,
      DatabaseProvider.COLUMN_PURCHASE_DETAIL_QUANTITY: qty,
    };

    if (id != null) {
      map[DatabaseProvider.TABLE_PURCHASE_DETAIL] = id;
    }

    return map;
  }

  PurchaseDetail.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseProvider.COLUMN_PURCHASE_DETAIL_ID];
    purchaseId = map[DatabaseProvider.COLUMN_PURCHASE_DETAIL_PURCHASE_ID];
    productId = map[DatabaseProvider.COLUMN_PURCHASE_DETAIL_PRODUCT_ID];
    qty = map[DatabaseProvider.COLUMN_PURCHASE_DETAIL_QUANTITY];
  }
}
