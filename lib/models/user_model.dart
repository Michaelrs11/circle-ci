import 'package:circleciapp/database/database_provider.dart';

class User {
  int id;
  String username, password, role, email;
  bool isActive;
  String activationLink, forgotPassworsLink;

  User(
      {this.id,
      this.username,
      this.password,
      this.role,
      this.email,
      this.isActive,
      this.activationLink,
      this.forgotPassworsLink});

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      DatabaseProvider.COLUMN_USERNAME: username,
      DatabaseProvider.COLUMN_USER_PASSWORD: password,
      DatabaseProvider.COLUMN_USER_ROLE: role,
      DatabaseProvider.COLUMN_USER_EMAIL: email,
      DatabaseProvider.COLUMN_USER_ISACTIVE: (isActive) ? "1" : "0",
      DatabaseProvider.COLUMN_USER_ACTIVATIONLINK: activationLink,
      DatabaseProvider.COLUMN_USER_FORGOTPASSWORD: forgotPassworsLink,
    };

    if (id != null) {
      map[DatabaseProvider.COLUMN_USER_ID] = id;
    }

    return map;
  }

  User.fromMap(Map<String, dynamic> map) {
    id = map[DatabaseProvider.COLUMN_USER_ID];
    username = map[DatabaseProvider.COLUMN_USERNAME];
    password = map[DatabaseProvider.COLUMN_USER_PASSWORD];
    role = map[DatabaseProvider.COLUMN_USER_ROLE];
    email = map[DatabaseProvider.COLUMN_USER_EMAIL];
    isActive =
        (map[DatabaseProvider.COLUMN_USER_ISACTIVE] == "1") ? true : false;
    activationLink = map[DatabaseProvider.COLUMN_USER_ACTIVATIONLINK];
    forgotPassworsLink = map[DatabaseProvider.COLUMN_USER_FORGOTPASSWORD];
  }
}
