import 'package:circleciapp/bloc/user_bloc.dart';
import 'package:circleciapp/database/database_provider.dart';
import 'package:circleciapp/events/user/init_user.dart';
import 'package:circleciapp/models/user_model.dart';
import 'package:circleciapp/screens/home_screen.dart';
import 'package:circleciapp/screens/welcome_screen.dart';
import 'package:dbcrypt/dbcrypt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences prefs;

  String _name = "";
  String _pass = "";

  List<User> _userList = [];

  @override
  void initState() {
    super.initState();
    DatabaseProvider.db.getUser().then(
      (userList) {
        BlocProvider.of<UserBloc>(context).add(InitUser(userList));
        _userList = userList;
      },
    );
    _initPrefs();
    _checkLoginState();
  }

  Future _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future _checkLoginState() async {
    for (var user in _userList) {
      if (prefs.containsKey(user.username + "isLogin") == true) {
        if (prefs.getBool(user.username + "isLogin") == true) {
          prefs.setString("logedInUsername", user.username);
          prefs.setInt("logedInId", user.id);

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => new WelcomeScreen(),
            ),
          );
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/background.jpg'),
            fit: BoxFit.cover,
          ),
        ),
         child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20.0,
            vertical: 50.0,
          ),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Center(
                  child: Text(
                'Login',
                style: TextStyle(fontSize: 50.0, fontWeight: FontWeight.bold),
              )),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Username',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(top: 14.0),
                        prefixIcon: Icon(
                          Icons.account_circle,
                          color: Colors.black,
                        )),
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Username field must be filled';
                      } else {
                        return null;
                      }
                    },
                    onChanged: (String value) {
                      _name = value;
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Password',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(top: 14.0),
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.black,
                        )),
                    obscureText: true,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Password field must be filled';
                      } else {
                        return null;
                      }
                    },
                    onChanged: (String value) {
                      _pass = value;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              InkWell(
                onTap: () {
                  submit();
                },
                child: Container(
                  height: 50.0,
                  decoration: new BoxDecoration(
                    color: Colors.blueAccent,
                    border: new Border.all(color: Colors.blue, width: 2.0),
                    borderRadius: new BorderRadius.circular(10.0),
                  ),
                  child: Center(
                    child: Text(
                      "Sign In",
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      ),
    );
  }

  void submit() async {
    for (var user in _userList) {
      if (_name == user.username) {
        print("Pass: " + _pass + " : " + "Hashed: " + user.password);
        print("is Same: " +
            new DBCrypt().checkpw(_pass, user.password).toString());

        if (new DBCrypt().checkpw(_pass, user.password)) {
          prefs.setBool(_name + "isLogin", true);
          prefs.setString("logedInUsername", _name);
          prefs.setInt("logedInId", user.id);

          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => new HomeScreen(),
            ),
          );
        } else {
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
              "Invalid username or Password!!!",
              style: TextStyle(color: Colors.red),
            ),
          ));
          return;
        }
      }
    }
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Username not Registered!!",
          style: TextStyle(color: Colors.red)),
    ));
  }
}
