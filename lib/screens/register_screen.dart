import 'package:circleciapp/bloc/user_bloc.dart';
import 'package:circleciapp/database/database_provider.dart';
import 'package:circleciapp/events/user/add_user.dart';
import 'package:circleciapp/events/user/init_user.dart';
import 'package:circleciapp/models/user_model.dart';
import 'package:circleciapp/screens/welcome_screen.dart';
import 'package:dbcrypt/dbcrypt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences prefs;

  String _name = "";
  String _email = "";
  String _pass = "";
  String _confPass = "";

  List<User> _userList = [];

  @override
  void initState() {
    super.initState();
    DatabaseProvider.db.getUser().then(
      (userList) {
        BlocProvider.of<UserBloc>(context).add(InitUser(userList));
        _userList = userList;
      },
    );
    _initPrefs();
  }

  Future _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/background.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20.0,
            vertical: 50.0,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                    child: Text(
                  'Register',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 50.0),
                )),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Username',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(top: 14.0),
                        prefixIcon: Icon(
                          Icons.account_circle,
                          color: Colors.black,
                        ),
                      ),
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Username field must be filled';
                        } else {
                          return null;
                        }
                      },
                      onChanged: (String value) {
                        _name = value;
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Email',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(top: 14.0),
                        prefixIcon: Icon(
                          Icons.mail,
                          color: Colors.black,
                        ),
                      ),
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Email field must be filled';
                        } else {
                          return null;
                        }
                      },
                      onChanged: (String value) {
                        _email = value;
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Password',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(top: 14.0),
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.black,
                        ),
                      ),
                      obscureText: true,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Password field must be filled';
                        } else {
                          return null;
                        }
                      },
                      onChanged: (String value) {
                        _pass = value;
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                        labelText: 'Confirm Password',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(top: 14.0),
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.black,
                        ),
                      ),
                      obscureText: true,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Confirm Password field must be filled';
                        } else {
                          return null;
                        }
                      },
                      onChanged: (String value) {
                        _confPass = value;
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                InkWell(
                  onTap: () {
                    submit();
                  },
                  child: Container(
                    height: 50.0,
                    decoration: new BoxDecoration(
                      color: Colors.blueAccent,
                      border: new Border.all(color: Colors.blue, width: 2.0),
                      borderRadius: new BorderRadius.circular(10.0),
                    ),
                    child: Center(
                        child: Text(
                      "Register",
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    )),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void submit() async {
    for (var user in _userList) {
      if (_name == user.username) {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text("Username is already taken"),
          ),
        );
        return;
      }

      if (_email == user.email) {
        _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text("Email is already taken"),
          ),
        );
        return;
      }
    }

    if (_confPass != _pass) {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Confirmation Password didn't match with the Password"),
        ),
      );
      return;
    }

    var hashedPassword = new DBCrypt().hashpw(_pass, new DBCrypt().gensalt());

    print(hashedPassword);

    User user = User(
        username: _name,
        password: hashedPassword,
        role: "User",
        email: _email,
        isActive: false,
        activationLink: "$_name $_email",
        forgotPassworsLink: "$_email $_name");

    DatabaseProvider.db.insertUser(user).then((newUser) {
      BlocProvider.of<UserBloc>(context).add(
        AddUser(newUser),
      );
      prefs.setBool(_name + "isLogin", true);
      prefs.setString("logedInUsername", _name);
      prefs.setInt("logedInId", newUser.id);
    });

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => new WelcomeScreen(),
      ),
    );
  }
}
