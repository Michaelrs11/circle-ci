import 'package:circleciapp/screens/dashboard_screen.dart';
import 'package:circleciapp/screens/drawer.dart';
import 'package:circleciapp/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String nama = "";

  SharedPreferences prefs;

  Future _checkLogin() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("logedInUsername", nama);
    if (pref.getBool("isLogin") == false) {
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => new Login()));
    }
  }

  Future _checkUser() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("logedInUsername") != null) {
      setState(() {
        nama = pref.getString("logedInUsername");
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _checkUser();
    _checkLogin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home Page"),
        centerTitle: true,
      ),
      drawer: DrawerMenu(),
      body: Padding(
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                "Welcome, " + nama,
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Text(
                "Circle CI is an app that provide you all service in one, such as food, bill, transfer, etc.We serve you with smile, honor, for us, your satisfaction is our priority",
                style: TextStyle(fontSize: 20.0, fontFamily: 'Lobster'),
                textAlign: TextAlign.justify,
              ),
              SizedBox(
                height: 50.0,
              ),
              _buttonStartNow(context),
            ],
          ),
        ),
      ),
    );
  }
}

Widget _buttonStartNow(BuildContext context) {
  return Padding(
    padding: EdgeInsets.all(8.0),
    child: new InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => new DashboardScreen()));
      },
      child: new Container(
        height: 50.0,
        decoration: new BoxDecoration(
          color: Colors.blueAccent,
          border: new Border.all(color: Colors.blueAccent, width: 2.0),
          borderRadius: new BorderRadius.circular(10.0),
        ),
        child: new Center(
          child: new Text(
            'Start Now',
            style: new TextStyle(fontSize: 18.0, color: Colors.white),
          ),
        ),
      ),
    ),
  );
}
