import 'package:circleciapp/screens/drawer.dart';
import 'package:flutter/material.dart';

class ContactUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Contact Us',
        ),
        centerTitle: true,
      ),
     drawer: DrawerMenu(),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 50,
            ),
            Container(
              width: 200.0,
              height: 200.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage(
                      'assets/images/logo.jpg',
                    ),
                    fit: BoxFit.fill),
              ),
            ),
            SizedBox(height: 20.0,),
            Text('Please Contact to: ', style: TextStyle(fontSize: 18.0,fontWeight: FontWeight.w500),),
            SizedBox(height: 20.0,),
            Text('Circle_CI@gmail.com', style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.bold),)
          ],
        ),
      ),
    );
  }
}
