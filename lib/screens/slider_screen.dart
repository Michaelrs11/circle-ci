import 'package:carousel_slider/carousel_slider.dart';
import 'package:circleciapp/screens/cards/food_card.dart';
import 'package:circleciapp/screens/drawer.dart';
import 'package:circleciapp/screens/recomendation_screen.dart';
import 'package:flutter/material.dart';

final List<String> imgList = [
  'https://upload.wikimedia.org/wikipedia/commons/0/0b/RedDot_Burger.jpg',
  'https://www.simplyrecipes.com/wp-content/uploads/2019/09/easy-pepperoni-pizza-lead-4-731x1024.jpg',
  'https://akcdn.detik.net.id/community/media/visual/2019/04/24/de2758a6-ea38-4ae9-8c4b-f2b395a81a22_43.png?w=700&q=90',
  'https://bayoranteknik.co.id/wp-content/uploads/2019/03/44570118_221335555434329_8693684617482036160_n-1024x1024.jpg',
  'https://upload.wikimedia.org/wikipedia/commons/3/3e/Nasi_goreng_indonesia.jpg'
];

final List<Widget> imageSliders = imgList
    .map((item) => Container(
          child: Container(
            margin: EdgeInsets.all(5.0),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                child: Stack(
                  children: <Widget>[
                    Image.network(item, fit: BoxFit.cover, width: 1000.0),
                    Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromARGB(200, 0, 0, 0),
                              Color.fromARGB(0, 0, 0, 0)
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                        child: Text(
                          'Food No. ${imgList.indexOf(item)+1}',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ))
    .toList();

class SliderScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SliderScreenState();
  }
}

class _SliderScreenState extends State<SliderScreen> {
  int _current = 0;
  int _selectedTab = 0;

   final List<FoodCard> learningFoodList = [
    FoodCard(
        image:
            "https://upload.wikimedia.org/wikipedia/commons/0/0b/RedDot_Burger.jpg",
        name: "Food 1",
        rate: 5,
        desc: "Delicious Food"),
    FoodCard(
        image:
            "https://www.simplyrecipes.com/wp-content/uploads/2019/09/easy-pepperoni-pizza-lead-4-731x1024.jpg",
        name: "Food 2",
        rate: 5,
        desc: "Delicious Food"),
    FoodCard(
        image:
            "https://akcdn.detik.net.id/community/media/visual/2019/04/24/de2758a6-ea38-4ae9-8c4b-f2b395a81a22_43.png?w=700&q=90",
        name: "Food 3",
        rate: 5,
        desc: "Delicious Food"),
  ];

  final List<FoodCard> completionFoodList = [
    FoodCard(
        image:
            "https://bayoranteknik.co.id/wp-content/uploads/2019/03/44570118_221335555434329_8693684617482036160_n-1024x1024.jpg",
        name: "Food 4",
        rate: 5,
        desc: "Delicious Food"),
    FoodCard(
        image:
            "https://upload.wikimedia.org/wikipedia/commons/3/3e/Nasi_goreng_indonesia.jpg",
        name: "Food 5",
        rate: 5,
        desc: "Delicious Food"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('My Food'),centerTitle: true,  actions: <Widget>[Icon(Icons.notifications)],),
      drawer: DrawerMenu(),
      body: Column(
        children: [
          InkWell(
            onTap: (){
              Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => new RecomendationScreen()));
            },
                      child: CarouselSlider(
              items: imageSliders,
              options: CarouselOptions(
                autoPlay: true,
                enlargeCenterPage: true,
                aspectRatio: 2.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: imgList.map((url) {
              int index = imgList.indexOf(url);
              return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: _current == index
                    ? Color.fromRGBO(0, 0, 0, 0.9)
                    : Color.fromRGBO(0, 0, 0, 0.4),
                ),
              );
            }).toList(),
          ),
          SizedBox(height: 20.0,),
          Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      InkWell(
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Text("Continue Making Food", style: TextStyle(fontWeight: FontWeight.bold),),
                            ),
                            SizedBox(height: 20.0),
                            Container(
                              height: 3.0,
                              width: MediaQuery.of(context).size.width * 0.30,
                              color: (_selectedTab == 0)
                                  ? Theme.of(context).primaryColor
                                  : Colors.white,
                            ),
                            SizedBox(height: 10.0),
                          ],
                        ),
                        onTap: () {
                          setState(() {
                            _selectedTab = 0;
                          });
                        },
                      ),
                      InkWell(
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Text("Making Queue",style: TextStyle(fontWeight: FontWeight.bold)),
                            ),
                            SizedBox(height: 10.0),
                            Container(
                              height: 3.0,
                              width: MediaQuery.of(context).size.width * 0.30,
                              color: (_selectedTab == 1)
                                  ? Theme.of(context).primaryColor
                                  : Colors.white,
                            ),
                            SizedBox(height: 20.0),
                          ],
                        ),
                        onTap: () {
                          setState(() {
                            _selectedTab = 1;
                          });
                        },
                      ),
                      InkWell(
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Text("Completion",style: TextStyle(fontWeight: FontWeight.bold)),
                            ),
                            SizedBox(height: 10.0),
                            Container(
                              height: 3.0,
                              width: MediaQuery.of(context).size.width * 0.30,
                              color: (_selectedTab == 2)
                                  ? Theme.of(context).primaryColor
                                  : Colors.white,
                            ),
                            SizedBox(height: 20.0),
                          ],
                        ),
                        onTap: () {
                          setState(() {
                            _selectedTab = 2;
                          });
                        },
                      ),
                    ],
                  ),
                  (_selectedTab == 0)
                      ? Container(
                          height: 300.0,
                          child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: completionFoodList.length,
                            scrollDirection: Axis.horizontal,
                            // physics: ClampingScrollPhysics(),
                            itemBuilder: (context, index) {
                              return completionFoodList[index];
                            },
                          ),
                        )
                      : (_selectedTab == 1)
                          ? Container(
                              height: 300.0,
                              child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: learningFoodList.length,
                                scrollDirection: Axis.horizontal,
                                // physics: ClampingScrollPhysics(),
                                itemBuilder: (context, index) {
                                  return learningFoodList[index];
                                },
                              ),
                            )
                          : Container(
                              height: 300.0,
                              child: ListView.builder(
                                shrinkWrap: true,
                                itemCount: completionFoodList.length,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, index) {
                                  return completionFoodList[index];
                                },
                              ),
                            ),
        ]
      ),
    );
  }
}