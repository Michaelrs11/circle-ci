import 'package:circleciapp/screens/drawer.dart';
import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'About',
        ),
        centerTitle: true,
      ),
      drawer: DrawerMenu(),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 15,
              ),
              Text(
                'Circle CI is an app that provides you all lives in one app such as food, transfer, mobile internet, etc.  ',
                style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.justify,
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                'Our Vision are: ',
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                textAlign: TextAlign.justify,
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                '1. Ensure our customers to provides the best service from Circle CI \n2. Make an app that makes life easier',
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.w300),
                textAlign: TextAlign.justify,
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                'Our Mission are: ',
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
                textAlign: TextAlign.justify,
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                '1. Create an app that user friendly for every generation such as millennials, and baby boomers. \n2. Our priority is customer satisfaction. ',
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.w300),
                textAlign: TextAlign.justify,
              ),
              SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
