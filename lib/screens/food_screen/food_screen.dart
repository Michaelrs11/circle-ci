import 'package:circleciapp/bloc/food_bloc.dart';
import 'package:circleciapp/database/database_provider.dart';
import 'package:circleciapp/events/foods/delete_food.dart';
import 'package:circleciapp/events/foods/init_food.dart';
import 'package:circleciapp/models/food_model.dart';
import 'package:circleciapp/screens/drawer.dart';
import 'package:circleciapp/screens/food_screen/cart_screen.dart';
import 'package:circleciapp/screens/food_screen/food_form.dart';
import 'package:circleciapp/screens/food_screen/transaction_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FoodScreen extends StatefulWidget {
  @override
  _FoodScreenState createState() => _FoodScreenState();
}

class _FoodScreenState extends State<FoodScreen> {
  List<int> _qtyList = [];
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    DatabaseProvider.db.getFood().then(
      (foodList) {
        BlocProvider.of<FoodBloc>(context).add(InitFood(foodList));
      },
    );
    _initPrefs();
  }

  Future _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  showFoodDialog(BuildContext context, Food food, int index) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        title: Text(food.name),
        content: Text("ID ${food.id}"),
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (context) => FoodForm(
                  food: food,
                  foodIdx: index,
                ),
              ),
            ),
            child: Text("Update"),
          ),
          FlatButton(
            onPressed: () => DatabaseProvider.db.delete(food.id).then((_) {
              BlocProvider.of<FoodBloc>(context).add(
                DeleteFood(index),
              );
              prefs.remove("Food" + food.id.toString());
              Navigator.pop(context);
            }),
            child: Text("Delete"),
          ),
          FlatButton(
            onPressed: () => Navigator.pop(context),
            child: Text("Cancel"),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 10.0,
        title: Text(
          'Food Screen',
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.history),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => new TransactionScreen(),
                ),
              );
            },
          ),
          IconButton(
            icon: Icon(Icons.shopping_cart),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => new CartScreen(),
                ),
              );
            },
          ),
        ],
      ),
      drawer: DrawerMenu(),
      body: Container(
        margin:
            EdgeInsets.only(left: 10.0, top: 10.0, bottom: 10.0, right: 0.0),
        child: BlocConsumer<FoodBloc, List<Food>>(
          builder: (context, foodList) {
            return ListView.separated(
              itemCount: foodList.length,
              itemBuilder: (BuildContext context, int index) {
                print("foodList: $foodList");

                Food food = foodList[index];

                _qtyList.add(0);

                return Stack(
                  children: <Widget>[
                    Card(
                      margin: EdgeInsets.only(right: 10.0),
                      elevation: 4.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(20.0),
                        ),
                      ),
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        child: ListTile(
                            title:
                                Text(food.name, style: TextStyle(fontSize: 30)),
                            subtitle: Text(
                              "Price: Rp. ${food.price}\nStock: ${food.stock}",
                              style: TextStyle(fontSize: 20),
                            ),
                            onTap: () => showFoodDialog(context, food, index)),
                      ),
                    ),
                    Positioned(
                      top: 10.0,
                      right: 0.0,
                      child: InkWell(
                        child: Container(
                          padding: EdgeInsets.symmetric(
                            vertical: 3.0,
                            horizontal: 8.0,
                          ),
                          decoration: BoxDecoration(
                              color: Colors.blue.withOpacity(0.7),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0))),
                          child: Icon(
                            (prefs.containsKey("Food" + food.id.toString()) ==
                                    true)
                                ? Icons.remove
                                : Icons.add,
                            color: Colors.white,
                            size: 20.0,
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            print(
                                prefs.containsKey("Food" + food.id.toString()));
                            print(prefs.containsKey("cekkk"));

                            if (prefs
                                    .containsKey("Food" + food.id.toString()) ==
                                false) {
                              prefs.setInt(
                                  "Food" + food.id.toString(), _qtyList[index]);
                            } else if (prefs
                                    .containsKey("Food" + food.id.toString()) ==
                                true) {
                              prefs.remove("Food" + food.id.toString());
                            }
                          });
                        },
                      ),
                    ),
                    Positioned(
                      top: 40.0,
                      right: 0.0,
                      child: Container(
                        height: 50.0,
                        width: 37.0,
                        decoration: BoxDecoration(
                          color: Colors.blue.withOpacity(0.7),
                          borderRadius: BorderRadius.all(
                            Radius.circular(10.0),
                          ),
                        ),
                        child: TextFormField(
                          initialValue:
                              (prefs.containsKey("Food" + food.id.toString()) ==
                                      true)
                                  ? prefs
                                      .getInt("Food" + food.id.toString())
                                      .toString()
                                  : _qtyList[index].toString(),
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(border: InputBorder.none, ),
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.number,
                          // decoration: InputDecoration(labelText: 'ID'),
                          onChanged: (String value) {
                            _qtyList[index] = int.parse(value);
                            if (prefs
                                    .containsKey("Food" + food.id.toString()) ==
                                true) {
                              prefs.setInt(
                                  "Food" + food.id.toString(), _qtyList[index]);
                            }
                          },
                        ),
                        // child: Text("test"),
                      ),
                    ),
                  ],
                );
              },
              separatorBuilder: (BuildContext context, int index) => SizedBox(
                height: 20.0,
              ),
            );
          },
          listener: (BuildContext context, foodList) {},
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (BuildContext context) => FoodForm()),
        ),
      ),
    );
  }
}
