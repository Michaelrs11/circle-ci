import 'package:circleciapp/bloc/food_bloc.dart';
import 'package:circleciapp/database/database_provider.dart';
import 'package:circleciapp/events/foods/add_food.dart';
import 'package:circleciapp/events/foods/update_food.dart';
import 'package:circleciapp/models/food_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FoodForm extends StatefulWidget {
  final Food food;
  final int foodIdx;

  FoodForm({this.food, this.foodIdx});

  @override
  _FoodFormState createState() => _FoodFormState();
}

class _FoodFormState extends State<FoodForm> {
  String _name;
  double _price;
  int _stock;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    if (widget.food != null) {
      _name = widget.food.name;
      _price = widget.food.price;
      _stock = widget.food.stock;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Food Form"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(30.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                TextFormField(
                  initialValue: _name,
                  decoration: InputDecoration(labelText: 'Name'),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Name field must be filled';
                    } else {
                      return null;
                    }
                  },
                  onSaved: (String value) {
                    _name = value;
                  },
                ),
                TextFormField(
                  initialValue: _price != null ? _price.toString() : null,
                  decoration: InputDecoration(labelText: 'Price'),
                  keyboardType: TextInputType.number,
                  validator: (String value) {
                    double price = double.tryParse(value);

                    if (price == null || price <= 0.0) {
                      print('Price must be greater than 1000.0');
                      return 'Price must be greater than 1000.0';
                    }

                    return null;
                  },
                  onSaved: (String value) {
                    _price = double.tryParse(value);
                  },
                ),
                TextFormField(
                  initialValue: _stock != null ? _stock.toString() : null,
                  decoration: InputDecoration(labelText: 'Stock'),
                  keyboardType: TextInputType.number,
                  validator: (String value) {
                    int stock = int.tryParse(value);

                    if (stock == null || stock <= 0) {
                      print('Stock must be greater than 0');
                      return 'Stock must be greater than 0';
                    }

                    return null;
                  },
                  onSaved: (String value) {
                    _stock = int.tryParse(value);
                  },
                ),
                widget.food == null
                    ? RaisedButton(
                        child: Text('Submit'),
                        onPressed: () {
                          if (!_formKey.currentState.validate()) {
                            return;
                          }

                          _formKey.currentState.save();

                          Food food = Food(
                            name: _name,
                            price: _price,
                            stock: _stock,
                          );

                          DatabaseProvider.db.insert(food).then(
                                (newFood) =>
                                    BlocProvider.of<FoodBloc>(context).add(
                                  AddFood(newFood),
                                ),
                              );

                          Navigator.pop(context);
                        },
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          RaisedButton(
                            child: Text('Update'),
                            onPressed: () {
                              if (!_formKey.currentState.validate()) {
                                return;
                              }

                              _formKey.currentState.save();

                              Food food = Food(
                                id: widget.food.id,
                                name: _name,
                                price: _price,
                                stock: _stock,
                              );

                              DatabaseProvider.db.update(food).then(
                                    (newFood) =>
                                        BlocProvider.of<FoodBloc>(context).add(
                                      UpdateFood(widget.foodIdx, food),
                                    ),
                                  );

                              Navigator.pop(context);
                            },
                          ),
                          RaisedButton(
                            child: Text('Cancel'),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
