import 'package:circleciapp/bloc/food_bloc.dart';
import 'package:circleciapp/bloc/purchase_bloc.dart';
import 'package:circleciapp/bloc/purchase_detail_bloc.dart';
import 'package:circleciapp/database/database_provider.dart';
import 'package:circleciapp/events/foods/init_food.dart';
import 'package:circleciapp/events/purchase/add_purchase.dart';
import 'package:circleciapp/events/purchase_detail/add_purchase_detail.dart';
import 'package:circleciapp/models/food_model.dart';
import 'package:circleciapp/models/purchase.dart';
import 'package:circleciapp/models/purchase_detail.dart';
import 'package:circleciapp/screens/drawer.dart';
import 'package:circleciapp/screens/food_screen/transaction_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BillScreen extends StatefulWidget {
  @override
  _BillScreenState createState() => _BillScreenState();
}

class _BillScreenState extends State<BillScreen> {
  SharedPreferences prefs;
  double _totalPrice = 0;
  int _detailCount = 0;
  List<int> _productIdList = [];

  @override
  void initState() {
    super.initState();
    _initPrefs();
    DatabaseProvider.db.getFood().then(
      (foodList) {
        BlocProvider.of<FoodBloc>(context).add(InitFood(foodList));
        for (var food in foodList) {
          if (prefs.containsKey("Food" + food.id.toString()) == true) {
            _totalPrice +=
                (food.price * prefs.getInt("Food" + food.id.toString()));
            _detailCount++;
            _productIdList.add(food.id);
          }
        }
      },
    );
  }

  Future _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 10.0,
        title: Text(
          'My Bill',
        ),
        centerTitle: true,
         actions: <Widget>[Icon(Icons.notifications)],
      ),
      drawer: DrawerMenu(),
      body: Card(
        elevation: 5.0,
        child: Container(
          child: Column(
            children: <Widget>[
              Text("BILL", style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold),),
              BlocConsumer<FoodBloc, List<Food>>(
                builder: (context, foodList) {
                  return Column(
                    children: <Widget>[
                      Container(
                        height: 400.0,
                        padding: EdgeInsets.symmetric(
                            horizontal: 30.0, vertical: 50.0),
                        child: ListView.separated(
                          itemCount: foodList.length,
                          itemBuilder: (BuildContext context, int index) {
                            print("foodList: $foodList");

                            Food food = foodList[index];

                            return (prefs.containsKey(
                                        "Food" + food.id.toString())) ==
                                    true
                                ? Text(
                                    "Item: ${food.name}\nQty: ${prefs.getInt("Food" + food.id.toString())} \nRp. ${food.price * prefs.getInt("Food" + food.id.toString())}", style: TextStyle(fontSize: 22.0),)
                                : null;
                          },
                          separatorBuilder: (BuildContext context, int index) =>
                              SizedBox(
                            height: 20.0,
                          ),
                        ),
                      ),
                      Text('Total: ${_totalPrice.toString()}',style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold,),)
                    ],
                  );
                },
                listener: (BuildContext context, foodList) {},
              ),
              SizedBox(
                height: 20.0,
              ),
              InkWell(
                child: Container(
                  width: 150.0,
                  padding: EdgeInsets.symmetric(
                    horizontal: 30.0,
                    vertical: 10.0,
                  ),
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.all(Radius.circular(20.0))),
                  child: Center(
                    child: Text(
                      "Pay",
                      style: TextStyle(color: Colors.white, fontSize: 20.0),
                    ),
                  ),
                ),
                onTap: () {
                  Purchase purchase = Purchase(
                    date: DateTime.now().toString(),
                    userId: 1,
                  );

                  DatabaseProvider.db.insertPurchase(purchase).then(
                    (newPurchase) {
                      BlocProvider.of<PurchaseBloc>(context).add(
                        AddPurchase(newPurchase),
                      );

                      for (int i = 0; i < _detailCount; i++) {
                        PurchaseDetail addedDetail = PurchaseDetail(
                          productId: _productIdList[i],
                          purchaseId: newPurchase.id,
                          qty: prefs.getInt(
                            "Food" + _productIdList[i].toString(),
                          ),
                        );

                        DatabaseProvider.db
                            .insertPurchaseDetail(addedDetail)
                            .then(
                          (newDetail) {
                            BlocProvider.of<PurchaseDetailBloc>(context).add(
                              AddPurchaseDetail(newDetail),
                            );
                          },
                        );
                      }
                    },
                  );

                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => new TransactionScreen(),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
