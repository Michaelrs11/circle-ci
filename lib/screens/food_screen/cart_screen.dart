import 'package:circleciapp/bloc/food_bloc.dart';
import 'package:circleciapp/database/database_provider.dart';
import 'package:circleciapp/events/foods/init_food.dart';
import 'package:circleciapp/models/food_model.dart';
import 'package:circleciapp/screens/drawer.dart';
import 'package:circleciapp/screens/food_screen/bill_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CartScreen extends StatefulWidget {
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
    DatabaseProvider.db.getFood().then(
      (foodList) {
        BlocProvider.of<FoodBloc>(context).add(InitFood(foodList));
      },
    );
    _initPrefs();

  }

  Future _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 10.0,
        title: Text(
          'My Cart',
        ),
        centerTitle: true,
         actions: <Widget>[Icon(Icons.notifications)],
      ),
     drawer: DrawerMenu(),
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
                left: 10.0, top: 10.0, bottom: 10.0, right: 0.0),
            child: BlocConsumer<FoodBloc, List<Food>>(
              builder: (context, foodList) {
                return Container(
                  height: 400,
                  child: ListView.separated(
                    itemCount: foodList.length,
                    itemBuilder: (BuildContext context, int index) {
                      print("foodList: $foodList");

                      Food food = foodList[index];

                      return (prefs.containsKey("Food" + food.id.toString()) ==
                              true)
                          ? Stack(
                              children: <Widget>[
                                Card(
                                  margin: EdgeInsets.only(right: 10.0),
                                  elevation: 4.0,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(20.0),
                                    ),
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.all(10.0),
                                    child: ListTile(
                                      title: Text(food.name,
                                          style: TextStyle(fontSize: 30)),
                                      subtitle: Text(
                                        "Price: Rp. ${food.price}\nQuantity: ${prefs.getInt("Food" + food.id.toString())}",
                                        style: TextStyle(fontSize: 20),
                                      ),
                                    ),
                                  ),
                                ),
                                Positioned(
                                  top: 10.0,
                                  right: 0.0,
                                  child: InkWell(
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                        vertical: 3.0,
                                        horizontal: 8.0,
                                      ),
                                      decoration: BoxDecoration(
                                          color: Colors.blue.withOpacity(0.7),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10.0))),
                                      child: Icon(
                                        (prefs.containsKey("Food" +
                                                    food.id.toString()) ==
                                                true)
                                            ? Icons.remove
                                            : Icons.add,
                                        color: Colors.white,
                                        size: 20.0,
                                      ),
                                    ),
                                    onTap: () {
                                      setState(
                                        () {
                                          prefs.remove(
                                              "Food" + food.id.toString());
                                        },
                                      );
                                    },
                                  ),
                                ),
                              ],
                            )
                          : null;
                    },
                    separatorBuilder: (BuildContext context, int index) =>
                        SizedBox(
                      height: 20.0,
                    ),
                  ),
                );
              },
              listener: (BuildContext context, foodList) {},
            ),
          ),
          InkWell(
            child: Container(
               height: 50.0,
               width: 150.0,
              padding: EdgeInsets.symmetric(
                horizontal: 10.0,
                vertical: 10.0,
              ),
              decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              child: Center(
                child: Text(
                  "CheckOut",
                  style: TextStyle(color: Colors.white, fontSize: 20.0),
                ),
              ),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => new BillScreen(),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}