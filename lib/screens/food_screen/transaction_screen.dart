import 'package:circleciapp/bloc/food_bloc.dart';
import 'package:circleciapp/bloc/purchase_bloc.dart';
import 'package:circleciapp/bloc/purchase_detail_bloc.dart';
import 'package:circleciapp/database/database_provider.dart';
import 'package:circleciapp/events/purchase/init_purchase.dart';
import 'package:circleciapp/events/purchase_detail/init_purchase_detail.dart';
import 'package:circleciapp/models/food_model.dart';
import 'package:circleciapp/models/purchase.dart';
import 'package:circleciapp/models/purchase_detail.dart';
import 'package:circleciapp/screens/drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TransactionScreen extends StatefulWidget {
  @override
  _TransactionScreenState createState() => _TransactionScreenState();
}

class _TransactionScreenState extends State<TransactionScreen> {
  SharedPreferences prefs;
  @override
  void initState() {
    super.initState();
    DatabaseProvider.db.getPurchase().then(
      (purchaseList) {
        BlocProvider.of<PurchaseBloc>(context).add(InitPurchase(purchaseList));
      },
    );

    DatabaseProvider.db.getPurchaseDetail().then(
      (detailList) {
        BlocProvider.of<PurchaseDetailBloc>(context)
            .add(InitPurchaseDetail(detailList));
      },
    );
    _initPrefs();
  }

  Future _initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }

  showPurchaseDetailDialog(BuildContext context, Purchase purchase, int index) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        child: BlocConsumer<PurchaseDetailBloc, List<PurchaseDetail>>(
          builder: (context, detailList) {
            return Container(
              height: 900.0,
              padding: EdgeInsets.symmetric(vertical: 10.0),
              child: ListView.separated(
                itemCount: detailList.length,
                itemBuilder: (BuildContext context, int index) {
                  PurchaseDetail detail = detailList[index];

                  return Container(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: Card(
                      elevation: 4.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(20.0),
                        ),
                      ),
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        child: ListTile(
                          title: BlocConsumer<FoodBloc, List<Food>>(
                            builder: (context, foodList) {
                              String foodName = "";

                              for (var food in foodList) {
                                if (food.id == detail.productId) {
                                  foodName = food.name;
                                }
                              }

                              return Text("Food Name: $foodName",
                                  style: TextStyle(fontSize: 20));
                            },
                            listener: (BuildContext context, foodList) {},
                          ),
                          subtitle: BlocConsumer<FoodBloc, List<Food>>(
                            builder: (context, foodList) {
                              for (var food in foodList) {
                                if (food.id == detail.productId) {
                                  return Text(
                                      "Qty: ${detail.qty}\nPrice/Item: Rp. ${food.price}",
                                      style: TextStyle(fontSize: 20));
                                }
                              }
                              return null;
                            },
                            listener: (BuildContext context, foodList) {},
                          ),
                        ),
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) => SizedBox(
                  height: 20.0,
                ),
              ),
            );
          },
          listener: (BuildContext context, foodList) {},
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 10.0,
        title: Text(
          'Transaction Screen',
        ),
        centerTitle: true,
        actions: <Widget>[Icon(Icons.notifications)],
      ),
      drawer: DrawerMenu(),
      body: BlocConsumer<PurchaseBloc, List<Purchase>>(
        builder: (context, purchaseList) {
          return ListView.separated(
            itemCount: purchaseList.length,
            itemBuilder: (BuildContext context, int index) {
              Purchase purchase = purchaseList[index];

              return Container(
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                child: Card(
                  elevation: 4.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(20.0),
                    ),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(10.0),
                    child: ListTile(
                        title: Text("Purchase Id: ${purchase.id}",
                            style: TextStyle(fontSize: 30)),
                        subtitle: Text(
                          "User Id: ${purchase.userId}\nPurchase Date: ${purchase.date}",
                          style: TextStyle(fontSize: 20),
                        ),
                        onTap: () =>
                            showPurchaseDetailDialog(context, purchase, index)),
                  ),
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) => SizedBox(
              height: 20.0,
            ),
          );
        },
        listener: (BuildContext context, foodList) {},
      ),
    );
  }
}
