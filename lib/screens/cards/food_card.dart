import 'package:flutter/material.dart';

class FoodCard extends StatelessWidget {

final String name, desc, image;
final double rate;

FoodCard({this.name,this.desc,this.rate,this.image});

List<IconData> rateStar =[];

_generateRate() {
    double ratePoint = rate;

    int i = 0;

    while (i < 5) {
      if (ratePoint > 0.5) {
        rateStar.add(Icons.star);
        ratePoint -= 1.0;
        i++;
      } else if (ratePoint == 0.5) {
        rateStar.add(Icons.star_half);
        ratePoint -= 0.5;
        i++;
      } else if (ratePoint < 0.5) {
        rateStar.add(Icons.star_border);
        i++;
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    _generateRate();
     return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Card(
            elevation: 3.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(20.0),
              ),
            ),
            color: Theme.of(context).secondaryHeaderColor,
            child: Container(
              height: 300.0,
              width: 200.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    name,
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 3.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        rateStar[0],
                        color: Colors.yellow,
                      ),
                      Icon(
                        rateStar[1],
                        color: Colors.yellow,
                      ),
                      Icon(
                        rateStar[2],
                        color: Colors.yellow,
                      ),
                      Icon(
                        rateStar[3],
                        color: Colors.yellow,
                      ),
                      Icon(
                        rateStar[4],
                        color: Colors.yellow,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 3.0,
                  ),
                  Text(
                    desc,
                    style: TextStyle(fontSize: 20.0),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: 0.0,
            child: Container(
              height: 200.0,
              width: 200.0,
              child: ClipRRect(
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
                child: Image.network(
                  image,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}