import 'package:circleciapp/screens/drawer.dart';
import 'package:circleciapp/screens/food_screen/food_screen.dart';
import 'package:circleciapp/screens/recomendation_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  
  SharedPreferences prefs;

  String _username;

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((value) {
      prefs = value;
      _username = prefs.getString("logedInUsername");
      if (_username == null) {
        _username = "";
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        centerTitle: true,
        actions: <Widget>[Icon(Icons.notifications)],
      ),
      drawer: DrawerMenu(),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 30.0,
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 20.0,
                ),
                child: Card(
                  elevation: 5.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(50.0),
                    ),
                  ),
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.search),
                        SizedBox(
                          width: 20.0,
                        ),
                        Expanded(
                          child: TextField(
                            decoration: InputDecoration(
                                border: InputBorder.none, hintText: "Search"),
                            onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => RecomendationScreen())),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 50.0),
                      child: Card(
                        elevation: 5.0,
                        child: Container(
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.trophy),
                                  Text("1"),
                                ],
                              ),
                              SizedBox(
                                height: 80.0,
                              ),
                              Text("$_username", style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.bold),),
                              Text("Total Points",style: TextStyle(fontWeight: FontWeight.bold)),
                              Row(
                                children: <Widget>[
                                  Icon(FontAwesomeIcons.coins),
                                  SizedBox(
                                    width: 20.0,
                                  ),
                                  Text("37950",style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      top: 0.0,
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                            "https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4-300x300.png"),
                        radius: 55.0,
                      ),
                    ),
                    Positioned(
                      top: 80.0,
                      child: CircleAvatar(
                        backgroundColor: Theme.of(context).primaryColor,
                        radius: 25.0,
                        child: Text(
                          "Lv.44",
                          style: TextStyle(
                            color: Theme.of(context).buttonColor,
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              _buttons(context),
            ],
          ),
        ),
      ),
    );
  }
}

GestureDetector _menus(BuildContext context, String image, String text, var navigate){
  return GestureDetector(
    onTap: () {
      if(navigate != null){
        Navigator.push(context, MaterialPageRoute(builder: (context) => navigate));
      }
    },
    child: Container(
      height: 80,
      width: 80,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Image.asset(image,height: 50,width: 50,),
          SizedBox(height: 5,),
          Text(text, style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),)
        ],
      ),
    ),
  );
}

Column _buttons(BuildContext context){
  return Column(
    children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _menus(context, 'assets/images/food.png', 'Food', FoodScreen()),
          _menus(context, 'assets/images/pulsa.png', 'Pulsa',null),
          _menus(context, 'assets/images/transfer.png', 'Transfer',null),
          _menus(context, 'assets/images/profile.jpg', 'Profile',null),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _menus(context, 'assets/images/top up.png', 'Top Up',null),
          _menus(context, 'assets/images/settings.png', 'Settings',null),
          _menus(context, 'assets/images/angry.jpg', 'Angry',null),
          _menus(context, 'assets/images/more.png', 'More',null),
        ],
      )
    ],
  );
}