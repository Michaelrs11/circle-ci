import 'package:circleciapp/screens/login_screen.dart';
import 'package:circleciapp/screens/register_screen.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.tealAccent,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/background.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircleAvatar(
              radius: 50.0,
              backgroundImage: AssetImage('assets/images/logo.jpg'),
            ),
            Text(
              'Circle CI',
              style: TextStyle(
                fontFamily: 'Lobster',
                fontSize: 35.0,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            Text(
              'All in one for you',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            Column(
              children: <Widget>[
                _buttonRegister(context),
                _buttonLogin(context),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget _buttonLogin(BuildContext context) {
  return Padding(
    padding: EdgeInsets.all(8.0),
    child: new InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => new Login()));
      },
      child: new Container(
        height: 50.0,
        decoration: new BoxDecoration(
          color: Colors.blueAccent,
          border: new Border.all(color: Colors.blueAccent,width: 2.0),
          borderRadius: new BorderRadius.circular(10.0),
        ),
        child: new Center(
          child: new Text(
            'Login',
            style: new TextStyle(fontSize: 18.0, color: Colors.white),
          ),
        ),
      ),
    ),
  );
}

Widget _buttonRegister(BuildContext context) {
  return Padding(
    padding: EdgeInsets.all(8.0),
    child: new InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => new RegisterScreen()));
      },
      child: new Container(
        height: 50.0,
        decoration: new BoxDecoration(
          color: Colors.blueAccent,
          border: new Border.all(color: Colors.blueAccent,width: 2.0),
          borderRadius: new BorderRadius.circular(10.0),
        ),
        child: new Center(
          child: new Text(
            'Register',
            style: new TextStyle(fontSize: 18.0, color: Colors.white),
          ),
        ),
      ),
    ),
  );
}