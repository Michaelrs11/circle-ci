import 'package:circleciapp/screens/about_us.dart';
import 'package:circleciapp/screens/contact_us.dart';
import 'package:circleciapp/screens/dashboard_screen.dart';
import 'package:circleciapp/screens/food_screen/transaction_screen.dart';
import 'package:circleciapp/screens/home_screen.dart';
import 'package:circleciapp/screens/login_screen.dart';
import 'package:circleciapp/screens/recomendation_screen.dart';
import 'package:circleciapp/screens/slider_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DrawerMenu extends StatefulWidget {
  @override
  _DrawerMenuState createState() => _DrawerMenuState();
}

class _DrawerMenuState extends State<DrawerMenu> {

  SharedPreferences prefs;

  String _username;

  @override
  void initState() {
    super.initState();

    SharedPreferences.getInstance().then((value) {
      prefs = value;
      _username = prefs.getString("logedInUsername");
      if (_username == null) {
        _username = "";
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              padding: EdgeInsets.all(20.0),
              color: Theme.of(context).primaryColor,
              child: Center(
                child: Column(
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: NetworkImage(
                              'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4-300x300.png',
                            ),
                            fit: BoxFit.fill),
                      ),
                    ),
                    SizedBox(height: 20.0,),
                    Text("$_username",style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
            ),
            ListTile(
              title: Text(
                'Home',
                style: TextStyle(fontSize: 20.0),
              ),
              leading: Icon(
                Icons.home,
                color: Colors.black,
              ),
              onTap: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new HomeScreen(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text(
                'Dashboard',
                style: TextStyle(fontSize: 20.0),
              ),
              leading: Icon(
                Icons.dashboard,
                color: Colors.black,
              ),
              onTap: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new DashboardScreen(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text(
                'Transaction',
                style: TextStyle(fontSize: 20.0),
              ),
              leading: Icon(
                Icons.attach_money,
                color: Colors.black,
              ),
              onTap: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new TransactionScreen(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text(
                'My Food',
                style: TextStyle(fontSize: 20.0),
              ),
              leading: Icon(
                FontAwesomeIcons.utensils,
                color: Colors.black,
              ),
              onTap: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new SliderScreen(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text(
                'Recomendation',
                style: TextStyle(fontSize: 20.0),
              ),
              leading: Icon(
                FontAwesomeIcons.thumbsUp,
                color: Colors.black,
              ),
              onTap: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new RecomendationScreen(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text(
                'About Us',
                style: TextStyle(fontSize: 20.0),
              ),
              leading: Icon(
                FontAwesomeIcons.mobileAlt,
                color: Colors.black,
              ),
              onTap: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new AboutUs(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text(
                'Contact Us',
                style: TextStyle(fontSize: 20.0),
              ),
              leading: Icon(
                Icons.contact_mail,
                color: Colors.black,
              ),
              onTap: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new ContactUs(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text(
                'Logout',
                style: TextStyle(fontSize: 20.0),
              ),
              leading: Icon(
                Icons.lock,
                color: Colors.black,
              ),
              onTap: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.setBool("isLogin", false);

                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => new Login(),
                  ),
                );
              },
            ),
          ],
        ),
    );
  }
}