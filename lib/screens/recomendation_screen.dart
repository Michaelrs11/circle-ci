import 'package:circleciapp/screens/cards/food_card.dart';
import 'package:circleciapp/screens/drawer.dart';
import 'package:flutter/material.dart';

class RecomendationScreen extends StatefulWidget {
  @override
  _RecomendationScreenState createState() => _RecomendationScreenState();
}

class _RecomendationScreenState extends State<RecomendationScreen> {
  
  final List<FoodCard> recomendation = [
    FoodCard(
        image:
            "https://upload.wikimedia.org/wikipedia/commons/0/0b/RedDot_Burger.jpg",
        name: "Food 1",
        rate: 5,
        desc: "Delicious Food"),
    FoodCard(
        image:
            "https://www.simplyrecipes.com/wp-content/uploads/2019/09/easy-pepperoni-pizza-lead-4-731x1024.jpg",
        name: "Food 2",
        rate: 5,
        desc: "Delicious Food"),
    FoodCard(
        image:
            "https://akcdn.detik.net.id/community/media/visual/2019/04/24/de2758a6-ea38-4ae9-8c4b-f2b395a81a22_43.png?w=700&q=90",
        name: "Food 3",
        rate: 5,
        desc: "Delicious Food"),
    FoodCard(
        image:
            "https://bayoranteknik.co.id/wp-content/uploads/2019/03/44570118_221335555434329_8693684617482036160_n-1024x1024.jpg",
        name: "Food 4",
        rate: 5,
        desc: "Delicious Food"),
    FoodCard(
        image:
            "https://upload.wikimedia.org/wikipedia/commons/3/3e/Nasi_goreng_indonesia.jpg",
        name: "Food 5",
        rate: 5,
        desc: "Delicious Food"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Recomendation Page"),
        centerTitle: true,
        actions: <Widget>[Icon(Icons.notifications)],
      ),
      drawer: DrawerMenu(),
      body: Container(
        child: Column(
          children: <Widget>[
            Text(
              "Food Recomendation for you",
              style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 20.0,),
            Container(
              height: 300.0,
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: recomendation.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context,index){
                return recomendation[index];
              },
            ),
          ),
          ],
        ),
      ),
    );
  }
}
