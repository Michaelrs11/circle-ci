import 'package:circleciapp/models/food_model.dart';
import 'package:circleciapp/models/purchase.dart';
import 'package:circleciapp/models/purchase_detail.dart';
import 'package:circleciapp/models/user_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider{
  static const String TABLE_FOOD = "food";
  static const String COLUMN_ID = "id";
  static const String COLUMN_NAME = "name";
  static const String COLUMN_PRICE = "price";
  static const String COLUMN_STOCK = "stock";

  static const String TABLE_USER = "TbUser";
  static const String COLUMN_USER_ID = "UserID";
  static const String COLUMN_USERNAME = "Username";
  static const String COLUMN_USER_PASSWORD = "PasswordUser";
  static const String COLUMN_USER_ROLE = "RoleUser";
  static const String COLUMN_USER_EMAIL = "Email";
  static const String COLUMN_USER_ISACTIVE = "IsActive";
  static const String COLUMN_USER_ACTIVATIONLINK = "ActivationLink";
  static const String COLUMN_USER_FORGOTPASSWORD = "ForgotPasswordLink";

  static const String TABLE_PURCHASE = "TbPurchase";
  static const String COLUMN_PURCHASE_ID = "PurchaseID";
  static const String COLUMN_PURCHASE_USER_ID = "UserID";
  static const String COLUMN_PURCHASE_DATE = "PurchaseDate";

  static const String TABLE_PURCHASE_DETAIL = "TbPurchaseDetail";
  static const String COLUMN_PURCHASE_DETAIL_ID = "PurchaseDetailID";
  static const String COLUMN_PURCHASE_DETAIL_PRODUCT_ID = "ProductID";
  static const String COLUMN_PURCHASE_DETAIL_QUANTITY = "Quantity";
  static const String COLUMN_PURCHASE_DETAIL_PURCHASE_ID = "PurchaseID";

  DatabaseProvider._();
  static final DatabaseProvider db = DatabaseProvider._();

  Database _database;

  Future<Database> get database async {
    print("database getter called");
    if (_database != null) {
      return _database;
    }

    _database = await createDatabase();

    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();
    print(dbPath);

    return await openDatabase(
      join(dbPath, 'circleCI.db'),
      version: 1,
      onCreate: (Database database, int version) async {
        print("Creating table");

        await database.execute(
          "CREATE TABLE $TABLE_FOOD ("
          "$COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT,"
          "$COLUMN_NAME TEXT,"
          "$COLUMN_PRICE INTEGER,"
          "$COLUMN_STOCK INTEGER"
          ")",
        );

        await database.execute(
          "CREATE TABLE $TABLE_USER ("
          "$COLUMN_USER_ID	INTEGER PRIMARY KEY AUTOINCREMENT,"
          "$COLUMN_USERNAME	TEXT NOT NULL UNIQUE,"
          "$COLUMN_USER_PASSWORD	TEXT NOT NULL,"
          "$COLUMN_USER_ROLE	TEXT NOT NULL,"
          "$COLUMN_USER_EMAIL	TEXT NOT NULL UNIQUE,"
          "$COLUMN_USER_ISACTIVE	TEXT NOT NULL DEFAULT 0,"
          "$COLUMN_USER_ACTIVATIONLINK	TEXT NOT NULL UNIQUE,"
          "$COLUMN_USER_FORGOTPASSWORD	TEXT UNIQUE"
          ")",
        );

        await database.execute(
          "CREATE TABLE $TABLE_PURCHASE ("
          "$COLUMN_PURCHASE_ID	INTEGER PRIMARY KEY AUTOINCREMENT,"
          "$COLUMN_PURCHASE_USER_ID	INTEGER NOT NULL,"
          "$COLUMN_PURCHASE_DATE	TEXT NOT NULL"
          ")",
        );

        await database.execute(
          "CREATE TABLE $TABLE_PURCHASE_DETAIL ("
          "$COLUMN_PURCHASE_DETAIL_ID	INTEGER PRIMARY KEY AUTOINCREMENT,"
          "$COLUMN_PURCHASE_DETAIL_PRODUCT_ID	INTEGER NOT NULL,"
          "$COLUMN_PURCHASE_DETAIL_QUANTITY	INTEGER NOT NULL,"
          "$COLUMN_PURCHASE_DETAIL_PURCHASE_ID	INTEGER NOT NULL"
          ")",
        );
      },
    );
  }

  // ==============================FOOD===========================

  Future<List<Food>> getFood() async {
    final db = await database;

    var foods = await db.query(TABLE_FOOD, columns: [
      COLUMN_ID,
      COLUMN_NAME,
      COLUMN_PRICE,
      COLUMN_STOCK
    ]);

    List<Food> foodList = List<Food>();

    foods.forEach((currentFood) {
      Food food = Food.fromMap(currentFood);

      foodList.add(food);
    });

    return foodList;
  }

  Future<Food> insert(Food food) async {
    final db = await database;
    food.id = await db.insert(TABLE_FOOD, food.toMap());
    return food;
  }

  Future<int> delete(int id) async {
    final db = await database;

    return await db.delete(
      TABLE_FOOD,
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<int> update(Food food) async {
    final db = await database;

    return await db.update(
      TABLE_FOOD,
      food.toMap(),
      where: "id = ?",
      whereArgs: [food.id],
    );
  }

  // ==============================USER===========================
  Future<List<User>> getUser() async {
    final db = await database;

    var users = await db.query(TABLE_USER, columns: [
      COLUMN_USER_ID,
      COLUMN_USERNAME,
      COLUMN_USER_PASSWORD,
      COLUMN_USER_ROLE,
      COLUMN_USER_EMAIL,
      COLUMN_USER_ISACTIVE,
      COLUMN_USER_ACTIVATIONLINK,
      COLUMN_USER_FORGOTPASSWORD,
    ]);

    List<User> userList = List<User>();

    users.forEach((currentUser) {
      User user = User.fromMap(currentUser);

      userList.add(user);
    });

    return userList;
  }

  Future<User> insertUser(User user) async {
    final db = await database;
    user.id = await db.insert(
      TABLE_USER,
      user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return user;
  }

  Future<int> deleteUser(int id) async {
    final db = await database;

    return await db.delete(
      TABLE_USER,
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<int> updateUser(User user) async {
    final db = await database;

    return await db.update(
      TABLE_USER,
      user.toMap(),
      where: "id = ?",
      whereArgs: [user.id],
    );
  }

  // ==============================PURCHASE===========================

  Future<List<Purchase>> getPurchase() async {
    final db = await database;

    var purchases = await db.query(TABLE_PURCHASE, columns: [
      COLUMN_PURCHASE_ID,
      COLUMN_PURCHASE_USER_ID,
      COLUMN_PURCHASE_DATE
    ]);

    List<Purchase> purchaseList = List<Purchase>();

    purchases.forEach((currentPurchase) {
      Purchase purchase = Purchase.fromMap(currentPurchase);

      purchaseList.add(purchase);
    });

    print(purchaseList);

    return purchaseList;
  }

  Future<Purchase> insertPurchase(Purchase purchase) async {
    final db = await database;
    purchase.id = (await db.insert(TABLE_PURCHASE, purchase.toMap()));
    return purchase;
  }

  Future<int> deletePurchase(int id) async {
    final db = await database;

    return await db.delete(
      TABLE_PURCHASE,
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<int> updatePurchase(Purchase purchase) async {
    final db = await database;

    return await db.update(
      TABLE_PURCHASE,
      purchase.toMap(),
      where: "id = ?",
      whereArgs: [purchase.id],
    );
  }

  // ==============================PURCHASE DETAIL===========================

  Future<List<PurchaseDetail>> getPurchaseDetail() async {
    final db = await database;

    var details = await db.query(TABLE_PURCHASE_DETAIL, columns: [
      COLUMN_PURCHASE_DETAIL_ID,
      COLUMN_PURCHASE_DETAIL_PRODUCT_ID,
      COLUMN_PURCHASE_DETAIL_PURCHASE_ID,
      COLUMN_PURCHASE_DETAIL_QUANTITY,
    ]);

    List<PurchaseDetail> detailList = List<PurchaseDetail>();

    details.forEach((currentDetail) {
      PurchaseDetail detail = PurchaseDetail.fromMap(currentDetail);

      detailList.add(detail);
    });
    print(detailList);
    return detailList;
  }

  Future<PurchaseDetail> insertPurchaseDetail(PurchaseDetail detail) async {
    final db = await database;
    detail.id = await db.insert(TABLE_PURCHASE_DETAIL, detail.toMap());
    return detail;
  }

  Future<int> deletePurchaseDetail(int id) async {
    final db = await database;

    return await db.delete(
      TABLE_PURCHASE_DETAIL,
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<int> updatePurchaseDetail(PurchaseDetail detail) async {
    final db = await database;

    return await db.update(
      TABLE_PURCHASE_DETAIL,
      detail.toMap(),
      where: "id = ?",
      whereArgs: [detail.id],
    );
  }

}