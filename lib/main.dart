import 'package:circleciapp/bloc/food_bloc.dart';
import 'package:circleciapp/bloc/purchase_bloc.dart';
import 'package:circleciapp/bloc/purchase_detail_bloc.dart';
import 'package:circleciapp/bloc/user_bloc.dart';
import 'package:circleciapp/screens/welcome_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<FoodBloc>(create: (context) => FoodBloc(),),
        BlocProvider<PurchaseBloc>(create: (context) => PurchaseBloc(),),
        BlocProvider<UserBloc>(create: (context) => UserBloc(),),
        BlocProvider<PurchaseDetailBloc>(create: (context) => PurchaseDetailBloc(),),
      ],
          child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
        ),
        home: WelcomeScreen(),
      ),
    );
  }
}