import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/events/foods/add_food.dart';
import 'package:circleciapp/events/foods/delete_food.dart';
import 'package:circleciapp/events/foods/init_food.dart';
import 'package:circleciapp/events/foods/update_food.dart';
import 'package:circleciapp/models/food_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FoodBloc extends Bloc<AppEvent, List<Food>> {
  @override
  List<Food> get initialState => List<Food>();

  @override
  Stream<List<Food>> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is InitFood) {
      yield event.foodList;
    } else if (event is AddFood) {
      List<Food> newState = List.from(state);
      if (event.newFood != null) {
        newState.add(event.newFood);
      }
      yield newState;
    } else if (event is DeleteFood) {
      List<Food> newState = List.from(state);
      newState.removeAt(event.foodIdx);
      yield newState;
    } else if (event is UpdateFood) {
      List<Food> newState = List.from(state);
      newState[event.foodIdx] = event.newFood;
      yield newState;
    }
  }
}
