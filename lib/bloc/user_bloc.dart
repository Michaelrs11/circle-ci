import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/events/user/add_user.dart';
import 'package:circleciapp/events/user/delete_user.dart';
import 'package:circleciapp/events/user/init_user.dart';
import 'package:circleciapp/events/user/update_user.dart';
import 'package:circleciapp/models/user_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserBloc extends Bloc<AppEvent, List<User>> {
  @override
  List<User> get initialState => List<User>();

  @override
  Stream<List<User>> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is InitUser) {
      yield event.userList;
    } else if (event is AddUser) {
      List<User> newState = List.from(state);
      if (event.newUser != null) {
        newState.add(event.newUser);
      }
      yield newState;
    } else if (event is DeleteUser) {
      List<User> newState = List.from(state);
      newState.removeAt(event.userIdx);
      yield newState;
    } else if (event is UpdateUser) {
      List<User> newState = List.from(state);
      newState[event.userIdx] = event.newUser;
      yield newState;
    }
  }
}
