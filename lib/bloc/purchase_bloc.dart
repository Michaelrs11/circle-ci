import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/events/purchase/add_purchase.dart';
import 'package:circleciapp/events/purchase/delete_purchase.dart';
import 'package:circleciapp/events/purchase/init_purchase.dart';
import 'package:circleciapp/events/purchase/update_purchase.dart';
import 'package:circleciapp/models/purchase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PurchaseBloc extends Bloc<AppEvent, List<Purchase>> {
  @override
  List<Purchase> get initialState => List<Purchase>();

  @override
  Stream<List<Purchase>> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is InitPurchase) {
      yield event.purchaseList;
    } else if (event is AddPurchase) {
      List<Purchase> newState = List.from(state);
      if (event.newPurchase != null) {
        newState.add(event.newPurchase);
      }
      yield newState;
    } else if (event is DeletePurchase) {
      List<Purchase> newState = List.from(state);
      newState.removeAt(event.purchaseIdx);
      yield newState;
    } else if (event is UpdatePurchase) {
      List<Purchase> newState = List.from(state);
      newState[event.purchaseIdx] = event.newPurchase;
      yield newState;
    }
  }
}
