import 'package:circleciapp/events/app_event.dart';
import 'package:circleciapp/events/purchase_detail/add_purchase_detail.dart';
import 'package:circleciapp/events/purchase_detail/delete_purchase_detail.dart';
import 'package:circleciapp/events/purchase_detail/init_purchase_detail.dart';
import 'package:circleciapp/events/purchase_detail/update_purchase_detail.dart';
import 'package:circleciapp/models/purchase_detail.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PurchaseDetailBloc extends Bloc<AppEvent, List<PurchaseDetail>> {
  @override
  List<PurchaseDetail> get initialState => List<PurchaseDetail>();

  @override
  Stream<List<PurchaseDetail>> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is InitPurchaseDetail) {
      yield event.detailList;
    } else if (event is AddPurchaseDetail) {
      List<PurchaseDetail> newState = List.from(state);
      if (event.newDetail != null) {
        newState.add(event.newDetail);
      }
      yield newState;
    } else if (event is DeletePurchaseDetail) {
      List<PurchaseDetail> newState = List.from(state);
      newState.removeAt(event.detailIdx);
      yield newState;
    } else if (event is UpdatePurchaseDetail) {
      List<PurchaseDetail> newState = List.from(state);
      newState[event.detailIdx] = event.newDetail;
      yield newState;
    }
  }
}